"""Contains objects that map to GURPS concepts related to characters, such as characters themselves, traits (advantages &
disadvantages), modifiers, skills, etc. This only contains the class definitions; Look to the gurps.database module for subclasses
representing content from sourcebooks."""


import math

import gurps.library.index

import gurps.core as core
import tables


class SkillCannotDefaultError(core.PyGURPSException): pass

damage_table = core.IncompleteTable({
                                    1 : ("1d-6", "1d-5"),
                                    2 : ("1d-6", "1d-5"),
                                    3 : ("1d-5", "1d-4"),
                                    4 : ("1d-5", "1d-4"),
                                    5 : ("1d-4", "1d-3"),
                                    6 : ("1d-4", "1d-3"),
                                    7 : ("1d-3", "1d-2"),
                                    8 : ("1d-3", "1d-2"),
                                    9 : ("1d-2", "1d-1"),
                                    10 : ("1d-2", "1d"),
                                    11 : ("1d-1", "1d+1"),
                                    12 : ("1d-1", "1d+2"),
                                    13 : ("1d 2", "d-1"),
                                    14 : ("1d", "2d"),
                                    15 : ("1d+1", "2d+1"),
                                    16 : ("1d+1", "2d+2"),
                                    17 : ("1d+2", "3d-1"),
                                    18 : ("1d+2", "3d"),
                                    19 : ("2d-1", "3d+1"),
                                    20 : ("2d-1", "3d+2"),
                                    21 : ("2d", "4d-1"),
                                    22 : ("2d", "4d"),
                                    23 : ("2d+1", "4d+1"),
                                    24 : ("2d+1", "4d+2"),
                                    25 : ("2d+2", "5d-1"),
                                    26 : ("2d+2", "5d"),
                                    27 : ("3d-1", "5d+1"),
                                    28 : ("3d-1", "5d+1"),
                                    29 : ("3d", "5d+2"),
                                    30 : ("3d", "5d+2"),
                                    31 : ("3d+1", "6d-1"),
                                    32 : ("3d+1", "6d-1"),
                                    33 : ("3d+2", "6d"),
                                    34 : ("3d+2", "6d"),
                                    35 : ("4d-1", "6d+1"),
                                    36 : ("4d-1", "6d+1"),
                                    37 : ("4d", "6d+2"),
                                    38 : ("4d", "6d+2"),
                                    39 : ("4d+1", "7d-1"),
                                    40 : ("4d+1", "7d-1"),
                                    45 : ("5d", "7d+1"),
                                    50 : ("5d+2", "8d-1"),
                                    55 : ("6d", "8d+1"),
                                    60 : ("7d-1", "9d"),
                                    65 : ("7d+1", "9d+2"),
                                    70 : ("8d", "10d"),
                                    75 : ("8d+2", "10d+2"),
                                    80 : ("9d", "11d"),
                                    85 : ("9d+2", "11d+2"),
                                    90 : ("10d", "12d"),
                                    95 : ("10d+2", "12d+2"),
                                    100 : ("11d", "13d")})

#@TODO: Add languages (should they be a trait? Or handled separately?)
#@TODO: Enforce rules about what skills a character is allowed to learn - see B168
class Character:
    """An actual GURPS character, complete with stats, skills, and traits."""

    def __init__(self,name="Noname"):
        self.name=name
        self.st=10
        self.dx=10
        self.iq=10
        self.ht=10
        self.tl=8
        self.traits=[]
        self.skills=[]

    def __repr__(self):
        return "<Character '{0}', {1} pts>".format(self.name,self.point_value())

    def __str__(self):
        statpoints, traitpoints, skillpoints = self.point_value(tabulated=True)
        totalpoints=statpoints+traitpoints+skillpoints

        statblock="{0.name}: {1} skills, {2} traits. ST: {0.st}, IQ: {0.iq}, DX: {0.dx}, HT: {0.ht}, SM: {0.sizemodifier}."
        pointblock="Point Value: {0} ({1} from stats, {2} from traits, {3} from skills.)"
        secondaryblock="Dmg: {0.dmg}, BL: {0.basiclift}, MaxHP: {0.maxhp}, MaxFP: {0.maxfp}, BS: {0.basicspeed} Bsc. Move: {0.basicmove}"

        strlist=[
              statblock.format(self,len(self.skills),len(self.traits)),
              pointblock.format(totalpoints,statpoints,traitpoints,skillpoints),
              secondaryblock.format(self)
        ]

        return "\n".join(strlist)

    @property
    def dmg(self):
        """Property that gives the character's basic melee damage. See B15. Note that this can be modified by the Striking ST trait.
        Return a tuple; [0] is thrusting damage, [1] is swinging damage. Both are in the form of dice+add strings, usable with string_roll()."""
        bonus_st=sum(self.fire_event("calculate_striking_st"))
        dmg=damage_table[self.st+bonus_st]
        return dmg

    @property
    def basiclift(self):
        """Property that gives how much weight, in pounds, the character can lift over their head with one hand in one second; (ST^2)/5
        This is rounded to the nearest integer if it's 10 lbs. or more."""
        bonus_st=sum(self.fire_event("calculate_lifting_st"))
        bl=(self.st+bonus_st)**2/5
        if bl >= 10:
            return(int(bl))
        else:
            return(bl)

    @property
    def maxhp(self):
        "Property that gives the character's maximum (base) HP. Equal to ST by default, but can be modified by traits."
        hp=self.st
        bonuses=self.fire_event("calculate_hp")
        bonus=sum(bonuses)
        return hp+bonus

    @property
    def will(self):
        "Property that gives the character's Will. Equal to IQ by default, but can be modified by traits."
        will=self.iq
        bonuses=self.fire_event("calculate_will")
        bonus=sum(bonuses)
        return will+bonus

    @property
    def perception(self):
        "Property that gives the character's Perception. Equal to IQ by default, but can be modified by traits."
        per=self.iq
        bonuses=self.fire_event("calculate_perception")
        bonus=sum(bonuses)
        return per+bonus

    @property
    def maxfp(self):
        """Property that gives the character's maximum (base) FP. Equal to HT by default, but can be modified by traits.
        FP's kind of a complex topic in GURPS. The base gurps.characters implementation does not take magic-only FP into account;
        see gurps.magic. The Machine trait will cause this property to be None; be careful!"""
        fp=self.ht
        bonuses=self.fire_event("calculate_fp")
        bonus=sum(bonuses)
        return fp+bonus

    @property
    def basicspeed(self):
        """Property that gives the character's Basic Speed (not the same as Basic Move - be careful!) Unlike most attributes, this is a
        float value. BS=(HT + DX) / 4"""
        bs=(self.ht + self.dx)/4
        bonuses=self.fire_event("calculate_basic_speed")
        bonus=sum(bonuses)
        return bs+bonus

    @property
    def basicmove(self):
        """Property that gives the character's Basic Move (do not confused with Basic Speed). This is the character's ground movement
        speed in yards per second. BM is BS rounded down."""
        bm=math.floor(self.basicspeed)
        bonuses=self.fire_event("calculate_basic_move")
        bonus=sum(bonuses)
        return bm+bonus

    @property
    def sizemodifier(self):
        """Property that gives the character's size modifier (SM); see B19. Note that this is worked out entirely through referencing traits."""
        smlist=self.fire_event("calculate_size_modifier")
        #I dunno why you'd ever have multiple traits that modify SM, but what the hell; sum them.
        return sum(smlist)

    def point_value(self,tabulated=False):
        """Property that gives the character's point value as an integer- the sum of the costs of all its stats, traits, and skills.
        If tabulated is true, returns a tuple of integers; [0] is the amount of points from stats, [1] is the amount of points from
        traits, [2] is the amount of points from skills. Note that secondary characteristic modifiers are filed as a trait, not a stat."""
        statpoints=0
        statpoints+=(self.st-10)*10
        statpoints+=(self.dx-10)*20
        statpoints+=(self.iq-10)*20
        statpoints+=(self.ht-10)*10

        traitpoints=sum((thetrait.pointvalue for thetrait in self.traits))

        skillpoints=sum((theskill.pointvalue for theskill in self.skills))

        if tabulated:
            return (statpoints,traitpoints,skillpoints)
        else:
            return statpoints+traitpoints+skillpoints

    def fire_event(self,eventname,*args,**kwargs):
        """Call Trait.process_event(eventname) for every trait in this character's .traits. Returns a list, which contains the value each event
        handler returned. Extra parameters given to this are passed down to the event handlers. See Trait.process_event for more info."""
        retlist=[]
        for thetrait in self.traits:
            retval=thetrait.process_event(eventname,*args,**kwargs)
            #Returning None represents "this trait doesn't care about this event".
            if retval is not None:
                retlist.append(retval)

        return retlist

    def find_default(self, skillname, returnall=False, nocrosstl=False):
        """Given the fullname of a skill (i.e. in Name/TL_ (Specialization) format), find what skills or stats this character has that it can default to.

        If returnall is True, returns a list of 2-tuples of (name, modifier) in decending order of the defaulting modifier;
        i.e. the list's [0]th element is the "best" default, the one that has the smallest penalty, and the last element is the "worst."

        The 'name' in the tuple is either the fullname of a skill, or the name of a stat; it's the same format as the against argument of
        Character.success_roll. Modifier is the modifier you want to add (usually a negative number) to the skill/stat's level because it's
        being used as a default.

        If returnall is not True, this just returns a tuple of (modifier, name) for the best default.

        If nocrosstl is True, this does not consider

        This uses find_skill, so if TL is unspecified, whatever TL of skill the character has is used; that's probably not what
        you really want, though. Leaving specialty unspecified only matches against no specialty.
        """
        basename, specialization, tl = core.parse_skill_name(skillname)
        theskillclass = gurps.library.index.look_up(superclasses=(Skill,), attrib_name=basename)

        if not theskillclass: raise gurps.library.TraitNotFoundError("{0} is not the name of a skill in any loaded book module.".format(basename))

        possibledefaults = []

        # If it's a technical skill, then the skill implicitly has itself at a different TL as a possible default; check that too.
        if theskillclass.technical:
            for defaultee in self.find_skill("{0} ({1})".format(basename, specialization), returnall=True):
                if tl:
                    crosstlpenalty = defaultee.cross_tl_penalty(tl)
                else:
                    #If TL is unspecified, we don't factor it in to anything; caller must be thinking about defaults in some abstract sense.
                    crosstlpenalty=0

                possibledefaults.append((defaultee.level, defaultee.fullname, crosstlpenalty))

        for thedefault in theskillclass.defaults:
            try:
                defaultsto, plusorminus, defaultmodifier = core.cascading_partition(thedefault, "-+")
            except TypeError:  # cascading_partition returned None, which you can't unpack
                # if there's no + or -,
                defaultsto = thedefault
                defaultmodifier = 0
            else:
                defaultmodifier = int(plusorminus + defaultmodifier)
            if defaultsto.lower() in {"st", "iq", "dx", "ht", "will", "perception"}:
                # possibledefaults is a list of (level, thedefault, defaultingmodifier) where thedefault is a valid argument to this function.
                statval = min(20, getattr(self, defaultsto.lower()))  # rule of 20; stat defaults treat stat as 20 or lower
                possibledefaults.append((statval, defaultsto.lower(), defaultmodifier))
            else:
                # If it's not a stat, must be a skill.
                defaultbasename, defaultspecialization, defaulttl = core.parse_skill_name(defaultsto)

                if defaultspecialization == "same": defaultspecialization = specialization

                if defaultspecialization == "any":
                    anyspec = True
                else:
                    anyspec = False

                # There may be multiple of the same skill with different TL
                # @TODO: Calc cross-tl penalties here too
                for defaultee in self.find_skill("{0} ({1})".format(defaultbasename, defaultspecialization), ignorespecialization=anyspec, returnall=True):
                    if tl:
                        crosstlpenalty = defaultee.cross_tl_penalty(tl)
                    else:
                        # If TL is unspecified, we don't factor it in to anything; caller must be thinking about defaults in some abstract sense.
                        crosstlpenalty = 0

                    possibledefaults.append((defaultee.level, defaultee.fullname, defaultmodifier + crosstlpenalty))

        if not possibledefaults:  # either we had none of the possible defaults or there were none in the first place.
            if theskillclass.defaults:
                raise SkillCannotDefaultError("Character has neither {0} nor any of its defaults ({1})".format(basename, theskillclass.defaults))
            else:
                raise SkillCannotDefaultError("Character does not have {0}, which has no defaults".format(basename, theskillclass.defaults))

        if returnall:
            #Normally sorted() sorts from low to high; we want high to low, so we set reversed=True
            return [ x[1:] for x in sorted(possibledefaults, key=lambda x: x[0] + x[2], reverse=True) ]
        else:
            return max(possibledefaults, key=lambda x: x[0] + x[2])[1:]

    def find_skill(self, skillname, specialization=None, tl=None, ignorespecialization=False, returnall=False):
        """Return the Skill object that represents this character's level of that skill.

        Respects specializations, i.e. 'Armoury (Guns)' doesn't match 'Armoury (Battlesuits)', and plain 'Armoury' never
        matches since Armoury requires a specialization, unless ignorespecialization is True in which case it just
        matches the plain name.

        Also respects /TL_ notation; the given TL is ignored when matching non-technical skills. If it's omitted,
        all TLs are matched.

        You can also use the specialization= and tl= keyword arguments; these override whatever's in the string.

        Returns the first entry in .skills that matches. Returns None if the character has no matching skill object.
        If returnall is true, returns a list of skills that match; if none match, returns an empty list.
        """
        basename, specialization_from_str, tl_from_str = core.parse_skill_name(skillname)
        if specialization is None: specialization = specialization_from_str
        if tl is None: tl = tl_from_str

        matches = lambda theskill: \
            theskill.name == basename \
            and (not theskill.technical or tl is None or theskill.tl == tl) \
            and (ignorespecialization or (not theskill.specialization or theskill.specialization == specialization))

        matching_skills = ( theskill for theskill in self.skills if matches(theskill) )

        if returnall:
            return list(matching_skills)
        else:
            return next(matching_skills, None)

    def success_roll(self, against, modifier=0, isdefault=False, dontdefault=False):
        """
        Make a stat, skill, or sense roll and return a gurps.RollResult named-tuple of (succeeded, wascritical, margin).

        All success rolls, that is, skill checks, sense checks, rolls against stats like HT or ST, the rolls in quick
        contests, etc... should be made through this function.

        modifier is simply the bonus/penalty to the roll; success_roll doesn't calculate this, although there are other
        methods that calculate bonus values in certain situations.

        against is a string that describes the thing being rolled against. To roll against a/an...

        skill: call success_roll with the name of the skill, usually just the skill's .name attribute.
               Supports specializations like Animal Handling (Big Cats), and \TL specifiers, calculating cross-TL penalties as needed.
               You'll still have to calculate unfamiliarity penalties yourself though (they stack with one another; see B169.)
               success_roll DOES roll against defaults if the character doesn't have the right skill, though.

               This uses find_skill, so if TL is unspecified, whatever TL of skill the character has is used; that's probably not what
               you really want, though. Leaving specialty unspecified only matches against no specialty.

               For example: call success_roll("Acrobatics") to roll against the character's Acrobatics, or
               call success_roll("Armoury (Battlesuit)", -2) to roll against the character's Armoury (Battlesuit)
               skill with a -2 penalty. If the character doesn't have the right skill, success_roll will
               then determine what default should be rolled against and roll against that.

        stat: call success_roll with the name of the stat. Also works on secondary stats, as you might imagine.
              Supported stats: 'st', 'iq', 'dx', 'ht', 'will', and 'perception'.
              For example: call success_roll("iq",+2) to roll against IQ+2, or call success_roll("ht",-1) to roll
              against HT at -1.

        sense: call success_roll with one of the following:
               "vision"
               "hearing"
               "smell"
               "taste"
               This will roll against Per and add the correct bonuses and maluses from traits, added to the given
               modifier value.

        Returns a core.RollResults, which is a named tuple of (bool, bool, int), where [0] is True for success, False
        for failure, [1] is True if the success/failure was critical. Or you can access the named tuple with
        .succeded, .wascritical, and .margin.

        You can even do cool stuff like this:
            if mycharacter.success_roll("st",-3).succeded:
                ...

        If against isn't one of the stat or sense strings, and isn't the name of a skill, you'll get a
        gurps.library.SkillNotFoundError.

        If a skill roll cannot be made because the character has none of the skill's defaults, characters.SkillCannotDefaultError is raised.

        isdefault is a flag that should be set to True if this is a roll against a default skill; this turns off further defaulting (see Double Defaults, B173)
        and engages the Rule of 20 (see Rule of 20, B173.)

        dontdefault turns off defaulting; isdefault implies dontdefault, so don't bother setting it in that case.

        On the subject of defaults, see http://forums.sjgames.com/showthread.php?t=37087 about why a skill with rlevel 0 or less doesn't count as "bought"
        for defaulting purposes.

        """
        if against.lower() in {"st", "iq", "dx", "ht", "will", "perception"}: #This is made case-insensitive for convenience when doing defaults, it isn't _really_ supported like that.
            statval = getattr(self, against.lower())
            if isdefault:
                statval = min(statval, 20) #rule of 20
            return core.success_roll( statval + modifier )
        elif against in {"vision", "hearing", "smell", "taste"}:
            pass
        #if it isn't one of those, maybe it's a skill name (if it isn't, the get_skill call will raise.)
        else:
            if isdefault: dontdefault=True

            #@TODO: Maybe have a dict that caches the character's skill names so we don't do a O(n) search every time?
            theskill=self.find_skill(against)

            if theskill is None:
                # If we don't have the skill, we'll have to roll against the defaults - which we need to look up.
                #We also want to look up the skill class even if dontdefault is on because we want the caller to know if it passed in a bad skill name
                if dontdefault: raise SkillCannotDefaultError("Character does not have correct skill, and dontdefault==True (probably because of double-default.)")

                # We have to figure out which possible default gives us the best chances; we'll only actually roll against that one.
                bestdefault, newmodifier = self.find_default(against)
                newmodifier += modifier

                return self.success_roll(bestdefault, newmodifier, isdefault=True)

            else:
                return theskill.roll(modifier)

    def string_success_roll(self,theroll, **kwargs):
        """Given a string like 'IQ-4' or "Armoury (Battlesuits)" or "Accounting+2", roll against this character's stat or skill and return a RollResult object
        or None if the roll cannot be made. Passes keyword arguments along to .success_roll.

        All this really does is split on + or - and call .success_roll with the results."""
        try:
            against, modifier = theroll.split("+")
            modifier=int(modifier)
        except IndexError:
            try:
                against, modifier = theroll.split("-")
                modifier = -int(modifier)
            except IndexError:
                #must be no modifier
                against = theroll.strip()
                modifier=0

        self.success_roll(against, modifier, **kwargs)



#@TODO: Add prerequisites
class Skill:
    """Represents the skill level of an individual character or character template. A TL MUST be given if the skill requires it;
    if it is omitted when needed the initializer will raise PyGURPSValueError. A subclass of this represents a skill as listed in a book, while
    an instance of that subclass represents an actual character's skill level."""

    _skillcosttable={
                    -3: 1,
                    -2: 2,
                    -1: 4,
                    0: 8,
                    1: 12,
                    2: 16,
                    3: 20,
                    4: 24,
                    5: 28}
    _skillcostlookup=core.IncompleteTable({v: k for k, v in _skillcosttable.items()})
    _difficultytoname={
                         0: "very hard",
                         1: "hard",
                         2: "average",
                         3: "easy"}
    _nametodifficulty={v: k for k, v in _difficultytoname.items()}

    name="*UNNAMED SKILL*"
    defaults=set()
    basestat="iq" #Note that basestat has to be lowercase (it has to be the same as the characters.Character attrib)
    _difficulty=1 #3="easy", 2="average", 1="hard", or 0="very hard"; it's stored as a magic number because we do math on it when calculating our point level
    reference="N/A" #The book and page this skill came from. B__ represents pages of the Basic Set; there's no real standard for these other than that.
    specializations=None #A list of strings, which are the book-listed specializations of the skill. Specializations aren't necessarily limited to this, however.
                                        #A skill that CAN'T have specializations will have None, if it can but none are listed for whatever reason use an empty tuple instead.
    mustspecialize=False #if this is true, the skill REQUIRES a specialization of some kind.
    technical=False #Whether or not this skill should have a TL.
    encumbrance_penalty=False #Whether or not the character's encumbrance level acts as a malus to this skill

    def __init__(self,rlevel,owner,tl=None,specialization=None):
        self.rlevel=rlevel
        self.owner=owner

        if self.technical: #This evaluates false if the skill doesn't need TL OR if the skill needs and has it.
            if tl is None:
                raise core.PyGURPSValueError("{0} is a technical skill, but no TL was given".format(self.name))
            self.tl=tl
        else:
            self.tl=None

        if self.mustspecialize and not specialization:
            raise core.PyGURPSValueError("{0} requires a specialization, but no specialization was given".format(self.name))
        self.specialization=specialization

    def __str__(self):
        return self.fullname

    @property
    def difficulty(self):
        "A property that gives a meaningful string value representing the skill's difficulty (stored internally as a magic number)."
        return self._difficultytoname[self._difficulty]

    @difficulty.setter
    def difficulty(self,newdiff):
        assert newdiff in ("easy","average","hard","very hard")
        self._difficulty=self._nametodifficulty[newdiff]

    @property
    def level(self):
        "A property that gives the actual level of the skill (owner's base + self.rlevel)"
        assert self.owner, "Tried to get the absolute level of a skill with invalid owner"
        #First we grab our base stat from our owner, whatever that base stat actually is
        base=getattr(self.owner,self.basestat)

        #Then we see if any traits (such as Talents) want to interfere - remember that event names are lowercase
        #and spaced with underscores
        eventname=self.name.lower().replace(" ","_")
        modifiers=self.owner.fire_event("calculate_skill_{0}".format(eventname))
        bonus=core.safe_sum(modifiers)

        #And our level is the base stat + our relative level + any bonuses from traits
        return base+self.rlevel+bonus

    @level.setter
    def level(self,newlevel):
        #First we get our old level - instead of looking at what kinds of modifiers and stuff are in play,
        #we just use the code above that does all that for us
        oldlevel=self.level
        leveldelta=oldlevel-newlevel
        self.rlevel-=leveldelta

    #@TODO: Implement "improving from defaults," see B173
    @property
    def pointvalue(self):
        """A property that represents how many points this skill is worth. If set, will change this skill's rlevel to make it worth
        the given number of points. The point value will "snap" to the lowest valid value (i.e. if basestat+0 is 1 point and +1 is 4,
        myskill.pointvalue=3 will actually set myskill.rlevel to 0 and myskill.pointvalue will thus be 1."""
        #The skill cost table just shifts up/down with difficulty

        effectiverlevel=self.rlevel-self._difficulty

        if self.rlevel > 5:
            #The skill cost table only goes up to 5; it's 4 per for every level above that
            return self._skillcosttable[5]*(self.rlevel-5)
        else:
            try:
                return self._skillcosttable[effectiverlevel]
            except KeyError:
                #@TODO: Deal with rlevel being too high and low properly; I dunno what I was thinking here
                #If our rlevel is -4 or less then we go off the OTHER end of the table; we're worth 1 then
                return 1

    @pointvalue.setter
    def pointvalue(self,newpv):
        #A skill at "very hard" at any given rlevel is the same price as a skill at "easy" at that rlevel +3;
        #thus just as our difficulty number shifts the cost table up and down converting rlevel to price,
        #converting price to rlevel shifts the rlevel itself by our difficulty
        self.rlevel=self._skillcostlookup[newpv]+self._difficulty

    @property
    def fullname(self):
        "A property that gives the 'full name' of the skill, with /TL_ if it is technical and (spec.) if it has a specialization."
        if self.technical:
            tlpart="/TL{0}".format(self.tl)
        else:
            tlpart=""

        if self.specialization:
            specpart=" ({0})".format(self.specialization)
        else:
            specpart=""

        return "{0}{1}{2}".format(self.name,tlpart,specpart)

    def roll(self,modifier=0):
        """Roll against this skill plus the given modifier (which can be negative). You probably want Character.success_roll for actually doing success
        rolls for a given character, though.

        Returns a core.RollResults named-tuple of bools and one int unless successonly is True; [0] is true if the roll indicated a success,
        [1] is true if the success/failure was critical, and [2] of the returned tuple is the margin of success/failure (roll - effective_skill)."""
        level=self.level
        success, crit, rollvalue=core.success_roll(level+modifier)

        return core.RollResults(success,crit,level-rollvalue)

    def cross_tl_penalty(self, othertl):
        """Given the other skill's TL, return the penalty incurred for using this skill as a substitute for it.

        Right now this assumes the other skill is the same as this one, and thus has the same base stat; this may change.
        """
        if self.basestat == "iq":
            return tables.iq_skill_tl_delta_to_modifier[othertl - self.tl]
        else:
            return -abs(othertl - self.tl)


class Trait:
    """Represents a specific trait - trait being the general noun covering advantages, disadvantages, perks, and quirks. The only
    difference, both in GURPS and in this code, is the point value. They can have modifiers attached to them, which affects
    both their point value and their behavior. Traits are perhaps the most difficult to formalize in code of the base GURPS
    concepts (though Magic spells are likely going to be even harder)"""

    #This denotes which trait modifiers make sense to put on this trait.
    #If this is an empty set, no modifiers are allowed.
    allowedmodifiers=set()

    #Since each actual trait is a subclass of Trait, a lot of stuff is defined at the class level
    name="*UNNAMED TRAIT*"
    basepointvalue=0 #The base cost of the trait. The actual point value depends on modifiers. Mutially exclusive with costtable.
    leveled=False #Whether or not this trait has levels; its effect and cost scale with level. Mutually exclusive with acceptsparenthetical.
    costperlevel=None #None represents "it's not leveled you dunce"
    acceptsparenthetical=False #represents that the trait accepts a parenthetical. .parenthetical will always be None if this is not True
    requiresparenthetical=False #Represents that the trait REQUIRES a parenthetical, e.x. Duty (...); you can't have a generic duty.
    acceptedparentheticals=set() #Represents what parentheticals are valid; if this is an empty set, all are allowed. Only has an effect if acceptsparenthetical is True, ofc.
    costtable=None #Dict mapping .parenthetical values to base point costs. This is only used if basepointvalue is None.
    reference="N/A" #Place in the GURPS library this trait came from; "B24" = "Basic Set, p.24", "Magic 89" = "Magic, p. 89". No validation is done on this.
    mental=False
    physical=False
    social=False
    exotic=False
    supernatural=False
    virtual=False #Virtual traits are traits that aren't actually from a GURPS book, such as the Extra [Secondary Stat] traits.

    def __init__(self,modifiers=None,parenthetical=None):
        #This makes a PickyList that will allow everything in self.allowedmodifiers, not one that contains anything.
        self.modifiers=core.PickyList(self.allowedmodifiers)
        if modifiers:
            self.modifiers.extend(modifiers)

        if self.leveled:
            self.level=int(parenthetical) if parenthetical is not None else 0
        else:
            self.level=None

            if self.acceptsparenthetical:
                if self.requiresparenthetical and parenthetical is None:
                    raise core.PyGURPSValueError("{0} requires a parenthetical, but none was given.".format(self.name))

                if self.acceptedparentheticals and parenthetical not in self.acceptedparentheticals:
                    raise core.PyGURPSValueError("'{0}' is not an accepted parenthetical for {1}.".format(parenthetical, self.name))

                self.parenthetical=parenthetical
            else:
                self.parenthetical=None


    def __repr__(self):
        return "<Trait '{0}' {1} points>".format(str(self),self.pointvalue)

    def __str__(self):
        if self.leveled:
            return "{0} ({1})".format(self.name,self.level)
        elif self.acceptsparenthetical:
            return "{0} ({1})".format(self.name,self.parenthetical)
        else:
            return self.name

    @property
    def pointvalue(self):
        if self.basepointvalue is None:
            retval=self.costtable[self.parenthetical]
        else:
            retval=self.basepointvalue

        if self.leveled and self.costperlevel:
            retval += (self.costperlevel * self.level)

        for themod in self.modifiers:
            amount, ispercent = themod.pointadj
            if ispercent:
                retval+=retval*(amount/100)
            else:
                retval+=amount

        return round(retval)

    def process_event(self,eventname,*args,**kwargs):
        """Check if this trait cares about the given trait event (for example, the Lifting ST trait cares about the calculate_basic_lift event).
        That is to say, it checks that the subclass (individual traits are subclasses of characters.Trait) defines a method with the same name
        as the event string; if it does, it calls that method, passing it *args and **kwargs, and returns the result that method returns.
        Otherwise, returns None.

        For a complete example, we will consider the calculate_will event. This event is fired by the characters.Character base class when
        it tries to work out what a character's Will is. It first does the computation for base Will (Will = IQ), then passes that number as the
        first argument to Character.fire_event, which calls process_event (this method) on all the character's traits with the same parameters it
        was given. Most of these traits will not have been written to do anything in response to calculate_basic_lift; however, the
        Extra Will virtual trait is, and this character has Extra Will. Extra Will's process_event will then call its calculate_basic_lift method,
        again passing the same parameters it was passed.

        Extra Will will then return the bonus Will points it represents. fire_event adds this return value to a list of all values returned
        by the event handlers it calls (assigning a meaning to and/or determining the meaning of those return values is left to the caller)
        which it will then itself return, and the character.will property will sum the base Will with these modifiers and represent that
        as the character's Will stat."""

        if hasattr(self,eventname):
            assert callable(getattr(self,eventname)), "Tried to process an event name that was the name of a non-callable attribute."
            return getattr(self,eventname)(*args,**kwargs)
        else:
            return None

class MetaTrait(Trait):
    """Metatraits are more or less just traits that imply the presence of other traits. For example, having the Machine meta-trait also
    means you have the Unhealing (Total) trait as well. Metatraits almost always have a point value equal to the sum of their parts. If
    you have, say, a 25-point metatrait that includes a 25-point trait, you pay 25 points; the price of the child trait is included in the
    price of the metatrait.

    Just like regular traits, each individual trait definition is a subclass of MetaTrait (and the actual objects that go into a character's
    .trait list is an instance of one of those subclasses). These subclasses should define subtrait_names as a list of names of traits;
    these will then be looked up with get_trait and added to the instance's .subtraits attribute at instantiation-time.
    """

    #This is NOT a list of trait objects or classes. This is a list of trait NAMES (i.e. the string "High Manual Dexterity")
    #The subtraits will be grabbed and added to self.subtraits at instantiation.
    subtrait_names=[]
    #If this is an int, the Metatrait's value is this.
    #If it's None, the metatrait's value is the sum of its subtraits'
    force_value=None

    def __init__(self,modifiers=None):
        #Should metatraits even have modifiers?
        super().__init__(modifiers)

        self.subtraits=[
                        gurps.library.index.get_trait(thetraitname,raise_on_no_results=True) for
                        thetraitname in
                        self.subtrait_names
                        ]

    @property
    def pointvalue(self):
        if self.force_value is not None:
            return self.force_value

        totalvalue=sum(thetrait.pointvalue for thetrait in self.subtraits)

        return totalvalue

class Perk(Trait):
    """A usually-custom-defined 1-point advantage (trait with positive cost) - perks & quirks will usually have no mechanical effect, but can add
    a little bit of definition to a character if used sparingly, and are handy for rounding out point values."""
    basepointvalue=1

    def __init__(self,customname="*UNNAMED PERK*"):
        super().__init__(None)
        self.name=customname

class Quirk(Trait):
    """A usually-custom-defined 1-point disadvantage (trait with negative cost) - perks & quirks will usually have no mechanical effect, but can add
    a little bit of definition to a character if used sparingly, and are handy for rounding out point values."""
    basepointvalue=-1

    def __init__(self,customname="*UNNAMED QUIRK*"):
        super().__init__(None)
        self.name=customname


class TraitModifier:
    """Represents the GURPS concept of trait modifiers: optional, modular bits of rules that modify the effects of traits
    and their point value."""

    name="[UNNAMED MODIFIER]"
    pointadj=(0,False)  #First part of this tuple is the amount to modify the trait's point value by, the second is True if this is a percentage
                        #modifier and False if it's just "add this many points"

if __name__ == "__main__":
    pass