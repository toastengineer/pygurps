'''
Library: GURPS Basic Set
Contains all the relevant content from the Basic Set - skills, traits, equipment, and the sample character sheets.
Spells are not included here - see gurps.library.magic.
'''

#Remember the trait subclass attributes:
#name="[UNNAMED TRAIT]"
#basepointvalue=0
#reference="N/A" #The book reference this trait comes from - usually the abbreviated name of the book followed by the page number
#mental=False
#physical=False
#social=False
#exotic=False
#supernatural=False
#virtual=False 
#Values that differ from the default don't need to be specified (i.e. non virtual traits don't need to define themselves as such; just leave it out)

import gurps.library.index
gurps.library.index.register_me()

import gurps.characters as characters

#ADVANTAGES

class ThreeSixtyVision(characters.Trait):
    name="360° Vision"
    basepointvalue=25
    physical=True
    exotic=True
    reference="B34"
    
    class EasyToHit(characters.TraitModifier):
        name="Easy To Hit"
        pointadj=(-20,True)
        
    allowedmodifiers={EasyToHit,}
        
class StrikingST(characters.Trait):
    name="Striking ST"
    basepointvalue=0 
    reference="B88"
    
    def __init__(self,parenthetical=0):
        super().__init__()
        self.level=int(parenthetical)
    
    def calculate_striking_st(self):
        return self.level
    
    @property
    def pointvalue(self):
        return self.level*5
    
class LiftingST(characters.Trait):
    name="Lifting ST"
    basepointvalue=0 
    reference="B65"
    
    def __init__(self,parenthetical=0):
        super().__init__()
        self.level=int(parenthetical)
    
    def calculate_lifting_st(self):
        return self.level
    
    @property
    def pointvalue(self):
        return self.level*3

class AcuteSenses(characters.Trait):
    name="Acute Senses"
    basepointvalue=0
    leveled=True
    costperlevel=2

class Ambidexterity(characters.Trait):
    name="Ambidexterity"
    basepointvalue=5

class AnimalEmpathy(characters.Trait):
    name="Animal Empathy"
    basepointvalue=5

class Catfall(characters.Trait):
    name="Catfall"
    basepointvalue=5

class CombatReflexes(characters.Trait):
    name="Combat Reflexes"
    basepointvalue=15

class DangerSense(characters.Trait):
    name="Danger Sense"
    basepointvalue=15

class Daredevil(characters.Trait):
    name="Daredevil"
    basepointvalue=15

class Empathy(characters.Trait):
    name="Empathy"
    basepointvalue=15

class EnhancedDefenses(characters.Trait):
    name="Enhanced Defenses"
    acceptsparenthetical=True
    requiresparenthetical=True

    costtable={
        "Enhanced Block": 5,
        "Enhanced Dodge": 15,
        "Enhanced Parry: All": 10
    }

    #@TODO: Extend this thing's __init__ so that if it's Enhanced Parry, it checks that its parenthetical is actually a weapon skill
    #@TODO: Make Enhanced Defenses (Enhanced Parry: {weaponskill}) work at all

    @property
    def basepointvalue(self):
        try:
            return self.costtable[self.parenthetical]
        except KeyError: #we must be Enhanced Parry: weaponskill...
            return 5

class Fearlessness(characters.Trait):
    name="Fearlessness"
    basepointvalue=0
    leveled=True
    costperlevel=2

class Flexibility(characters.Trait):
    name="Flexibility"
    basepointvalue=None
    acceptsparenthetical=True
    acceptedparentheticals={None, "Double-Jointed"}
    costtable={
        None: 5,
        "Double-Jointed": 15
    }

class HardToKill(characters.Trait):
    name="Hard To Kill"
    basepointvalue=0
    leveled=True
    costperlevel=2

class HighPainThreshold(characters.Trait):
    name="High Pain Threshold"
    basepointvalue=10

class Jumper(characters.Trait):
    name="Jumper"
    basepointvalue=100
    acceptsparenthetical=True
    requiresparenthetical=True
    acceptedparentheticals={"World", "Time"}

class LanguageTalent(characters.Trait):
    name="Language Talent"
    basepointvalue=10

class Luck(characters.Trait):
    name="Luck"
    basepointvalue=None
    acceptsparenthetical=True
    acceptedparentheticals={None, "Extraordinary", "Ridiculous"}
    costtable={
        None: 15,
        "Extraordinary": 30,
        "Ridiculous": 60
    }

class NightVision(characters.Trait):
    name="Night Vision"
    basepointvalue=0
    leveled=True
    costperlevel=1

class PerfectBalance(characters.Trait):
    name="Perfect Balance"
    basepointvalue=15

class Resistant(characters.Trait):
    name="Resistant"
    basepointvalue=None
    acceptsparenthetical=True
    requiresparenthetical=True
    acceptedparentheticals={"Disease: +3", "Disease: +8", "Poison"}
    costtable={
        "Disease: +3": 3,
        "Disease: +8": 5,
        "Poison" : 5
    }

#DISADVANTAGES

class BadSight(characters.Trait):
    name="Bad Sight"
    basepointvalue=None
    acceptsparenthetical=True
    acceptedparentheticals={None, "Correctable"}
    costtable={
        None: -25,
        "Correctable": -10
    }

class BadTemper(characters.Trait):
    name="Bad Temper"
    basepointvalue=-10

class Bloodlust(characters.Trait):
    name="Bloodlust"
    basepointvalue=-10

class CodeOfHonor(characters.Trait):
    name="Code of Honor"
    acceptsparenthetical=True
    requiresparenthetical=True
    predefinedcodes={
        "Pirate's": "Low",
        "Gentleman's": "Medium"
    }
    severitytovalue={
        "Low": -5,
        "Medium": -10,
        "High": -15
    }

    def __init__(self,modifiers=None,parenthetical=None,severity=None):
        super().__init__(modifiers,parenthetical)

        if not severity:
            if parenthetical in self.predefinedcodes:
                severity=self.predefinedcodes[parenthetical]
            else:
                severity="Medium"

        if severity not in ("Low", "Medium", "High"):
            raise gurps.PyGURPSValueError("Severity argument to CodeOfHonor constructor must be 'Low', 'Medium', or 'High'.")
        self.severity=severity

    @property
    def basepointvalue(self):
        return self.severitytovalue[self.severity]

class Curious(characters.Trait):
    name="Curious"
    basepointvalue=-5

class Delusions(characters.Trait):
    name="Delusions"
    basepointvalue=-5
    #@TODO: Do something like we have for CodeOfHonor for the severity of the delusions

class Gluttony(characters.Trait):
    name="Gluttony"
    basepointvalue=-5

class Greed(characters.Trait):
    name="Greed"
    basepointvalue=-15

class HardOfHearing(characters.Trait):
    name="Hard of Hearing"
    basepointvalue=-10

class Honesty(characters.Trait):
    name="Honesty"
    basepointvalue=-10

class Impulsiveness(characters.Trait):
    name="Impulsiveness"
    basepointvalue=-10

class Intolerance(characters.Trait):
    name="Intolerance"
    basepointvalue=-5
    acceptsparenthetical=True
    requiresparenthetical=True

class IntoleranceTotal(characters.Trait):
    name="Total Intolerance"
    basepointvalue=-10

class Jealousy(characters.Trait):
    name="Jealousy"
    basepointvalue=-10

class Lecherousness(characters.Trait):
    name="Lecherousness"
    basepointvalue=-15

class Curious(characters.Trait):
    name="Curious"
    basepointvalue=-5

class Obsession(characters.Trait):
    name="Obsession"
    basepointvalue=None
    acceptsparenthetical=True
    requiresparenthetical=True
    acceptedparentheticals={"Short-Term", "Long-Term"}
    costtable={
        "Short-Term": -5,
        "Long-Term": -10
    }

class Overconfidence(characters.Trait):
    name="Overconfidence"
    basepointvalue=-5

class Pacifism(characters.Trait):
    name="Pacifism"
    basepointvalue=-5
    #@TODO: Implement this

class Phobia(characters.Trait):
    name="Curious"
    basepointvalue=None
    #@TODO: Implement this

class SenseOfDuty(characters.Trait):
    name="Sense of Duty"
    basepointvalue=None
    #@TODO:Implement This

class Truthfullness(characters.Trait):
    name="Truthfullness"
    basepointvalue=-5

class Unluckiness(characters.Trait):
    name="Unluckiness"
    basepointvalue=-10

class Vow(characters.Trait):
    name="Vow"
    basepointvalue=-5
    #@TODO: Implement this

#METATRAITS

class Machine(characters.MetaTrait):
    name="Machine"

class Automaton(characters.MetaTrait):
    name="Automaton"

#SKILLS

class Accounting(characters.Skill):
    #Affected by Buisness Acumen and Mathematical Ability
    name="Accounting"
    defaults=("IQ-6","Finance-4","Mathematics (Statistics)-5","Merchant-5")
    basestat="iq"
    _difficulty=1
    reference="B174"

class Alchemy(characters.Skill):
    name="Alchemy"
    defaults=()
    basestat="iq"
    _difficulty=0
    reference="B174"
    technical=True
    
class AnimalHandling(characters.Skill):
    name="Animal Handling"
    defaults=("IQ-5",)
    basestat="iq"
    _difficulty=2
    reference="B175"
    mustspecialize=True
    specializations=["Big Cats","Dogs","Equines","Raptors"]
    
class Armoury(characters.Skill):
    name="Armoury"
    defaults=("IQ-5","Engineer (same)-4")
    basestat="iq"
    _difficulty=2
    reference="B178"
    technical=True
    mustspecialize=True
    specializations=["Battlesuits","Body Armor","Force Shields","Heavy Weapons","Missile Weapons","Small Arms","Vehicular Armor"]

class Engineer(characters.Skill):
    name="Engineer"
    basestat="iq"
    _difficulty=1
    reference="B190"
    technical=True
    mustspecialize=True
    specializations=['Artillery', 'Civil', 'Clockwork', 'Combat', 'Electrical', 'Electronics', 'Materials', 'Microtechnology', 'Mining', 'Nanotechnology', 'Parachronic', 'Psychotronics', 'Robotics', 'Small Arms', 'Temporal']

    specialization_to_defaults = {
        "Artillery" : {"Armoury (Heavy Weapons)-6"},
        "Civil" : {"Architecture-6"},
        "Clockwork" : {"Mechanic (same)-6"},
        "Combat" : {"Explosives (Demolition)-6"},
        "Electrical" : {"Electrician-6"},
        "Electronics" : {"Electronics Repair (any)-6"},
        "Microtechnology" : {"Mechanic(same)-6"},
        "Mining" : {"Explosives (Demolition)-6"},
        "Nanotechnology" : {"Mechanic (same)-6"},
        "Parachronic" : {"Electronics Operation (same)-6"},
        "Psychotronics" : {"Electronics Operation (same)-6"},
        "Robotics" : {"Mechanic (same)-6"},
        "Small Arms" : {"Armoury (same)-6"},
        "Temporal" : {"Electronics Operation (same)-6"},
    }

    @property
    def defaults(self):
        try:
            extradefaults = self.specialization_to_defaults[self.specialization]
        except KeyError:
            extradefaults = set()
        return {"Engineer (any)-4"} + extradefaults




#SHEETS

#EQUIPMENT
