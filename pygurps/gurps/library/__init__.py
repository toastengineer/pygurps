"""This package contains the 'databases' (called library modules) needed to run a GURPS game - modules containing Trait subclasses, mostly, as well 
as a few other things. This subpackage also holds the indexing system that allows searching of the library modules."""

from .index import *