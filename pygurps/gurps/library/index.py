'''
gurps.library.index allows a user to easily find content in the currently loaded GURPS library modules by name. This only checks modules
that have already been imported and have the _sourcebook attribute set to True.
'''
import importlib
import inspect
import pkgutil
import sys

import core
import gurps.core
import gurps.characters


#Note that librarymodules is in FIFO order with respect to imports; that is, the most recently imported library module
#will be at the top. This is very much intended; since look_up() searches down librarymodules, content from recently imported modules
#will "override" content from less recently imported modules (it'll all be there if you search in returnall mode of course).
librarymodules=[]

class PyGURPSLibraryError(gurps.core.PyGURPSException):
    "Base class for exceptions relating to the library or library.index systems."
class TraitNotFoundError(PyGURPSLibraryError):
    "Exception raised when get_trait fails to find a result when it is called with raise_on_no_results=True."
class SkillNotFoundError(PyGURPSLibraryError):
    "Exception raised when get_skill fails to find a result when it is called with raise_on_no_results=True."

def _filter(obj,filters):
    "Returns true if the given object matches ALL of the given filters."
    activefilters=filters.keys()
    if "superclasses" in activefilters:
        if not issubclass(obj,tuple(filters["superclasses"])):
            return False

    for thefilter in activefilters:
        if thefilter[:7]=="attrib_":
            attrib=thefilter[7:]
            value=filters[thefilter]
            if not (hasattr(obj,attrib) and getattr(obj,thefilter[7:]) == value):
                return False

    return True


def look_up(**kwargs):
    """Find the library item that matches the given set of filters.

    This will find anything on any library module that fits the filters given except things whose identifiers begin with _ or modules.
    By default, returns the first result that fits all the filters. A list of valid filters (given as keyword args) follows:

    reference=[name] : [name] must be a string. Return objects that are named with the given name in code (i.e. def blablabla()... would match refrence=blablabla)
    attrib_[attribname]=[values] : Attribname is part of the key. Return objects that have the given attribute set to one of the given values.
    modules=[modules] : [modules] must be a list of module objects. Only search the given modules.
    superclasses=[classlist] : classlist must be a list of classes. Only hand back objects that are subclasses of the given classes.
    returnall=True/False : If returnall is true, return a list of all results that fit the criteria.
    """

    if ("modules" in kwargs.keys()):
        allowedmodules=kwargs["modules"]
        modulestosearch=[themodule for themodule in librarymodules if themodule in allowedmodules]
    else:
        modulestosearch=librarymodules

    if kwargs.get("returnall"):
        returnall=True
        retlist=[]
    else:
        returnall=False

    for themodule in modulestosearch:
        for objname, obj in inspect.getmembers(themodule):
            if ("reference" in kwargs.keys()) and kwargs["reference"]!=objname:
                continue
            #We also dont want to process private things or imported modules
            if objname[0] == "_" or inspect.ismodule(obj):
                continue

            if _filter(obj,kwargs):
                if returnall:
                    retlist.append(obj)
                else:
                    return obj
    if returnall:
        return retlist
    else:
        #This'll only happen if there's no results at all
        return None

def get_trait(name,raise_on_no_results=False):
    """Search the entire index for a trait with the given string as its .name attribute (stripping anything in parentheses from the open
    parenthesis to the end of the string, i.e. Duty (ISWAT) is just Duty, but if you for some reason put in Bla (Bla) Bla it'd be reduced to "Bla").
    If found, instantiate it with the parenthetical, sans leading and trailing parentheses, (in the above example, the parentheticals would be
    "ISWAT" and "Bla) Bla") as the constructor's first parameter and return the result. The parenthetical will be converted to an int if
    parenthetical.isdecimal() results in True, or a float if parenthetical.isnumeric() results in True. Otherwise, it will be left as a str.
    In either case, the parenthetical is passed through the trait's __init__'s 'parenthetical' parameter.

    If no matching trait is found, return None, unless raise_on_no_results is True, in which case raise TraitNotFoundError.

    Remember that this returns a trait OBJECT, not a trait CLASS. I've made that mistake SO MANY TIMES."""

    parenoffst=name.find("(")
    parenthetical=""

    if parenoffst != -1:
        #we also .strip() them to remove the parentheses from the parenthetical and any trailing spaces from the name.
        parenthetical=name[parenoffst:].strip("()")
        name=name[:parenoffst].strip()

        #Int and float parentheticals are allowed (so Striking ST (8)) results in Striking ST with a level of 8)
        if parenthetical.isdecimal():
            parenthetical=int(parenthetical)
        elif parenthetical.isnumeric():
            parenthetical=float(parenthetical)

    resultcls=look_up(attrib_name=name,superclasses=[gurps.characters.Trait])

    if resultcls:
        if parenthetical:
            return resultcls(parenthetical=parenthetical)
        else:
            return resultcls()
    else:
        if raise_on_no_results:
            raise TraitNotFoundError("{0} is not the name of a trait in any loaded book module.".format(name))
        else:
            return None

def get_skill(name,rlevel=0, owner=None, raise_on_no_results=False):
    """Search the entire index for a skill with the given string as its .name attribute. If a trailing parenthetical is included, it will
    be considered as a specialization and stripped from the search string, then later have the skill instance's .specialization attribute
    set to it. If the substring "/TL_" - where _ is a number - is present, it will be considered a TL specifier and also stripped from the
    search string. The number will be provided as the tl argument to the skill's __init__. This does not check the validity of the instructions
    you give it; for example, you can give it just "Animal Handling" and it'll successfully find that skill - but Animal Handling requires a
    specialization, and because none was provided the skill object's __init__ will raise LibGURPSValueError.

    Takes the keyword parameters 'rlevel', which will be the .rlevel of the returned skill object, and 'owner' which will be the .owner
    of the returned skill object.

    Remember that this returns a skill OBJECT, NOT a CLASS."""

    basename, specialization, tl = core.parse_skill_name(name)

    resultcls=look_up(attrib_name=basename,superclasses=[gurps.characters.Skill])

    if resultcls:
        result=resultcls(rlevel,owner,tl,specialization)
        return result
    else:
        if raise_on_no_results:
            raise TraitNotFoundError("{0} is not the name of a skill in any loaded book module.".format(name))
        else:
            return None

def get_modifier(name,traitclass,casefold=True):
    """Search all the modifiers applicable to the given trait class and return one whose name is the same as the given name.
    Will return None if there is no match. If the casefold keyword argument is True, which it is by default, the names will be case-folded
    before comparison. This is highly recommended, as apparently-identical strings can compare false if not casefolded."""
    for themod in traitclass.allowedmodifiers:
        if casefold:
            if themod.name.casefold()==name.casefold():
                return themod()
        else:
            if themod.name.casefold()==name.casefold():
                return themod()
    return None


def register(module):
    "Register the given module as a library module."
    librarymodules.insert(0,module)

def register_me():
    "Register the calling module as a library module."
    callerstackframe = inspect.stack()[1]
    register(inspect.getmodule(callerstackframe[0]))

def load_all_library_modules():
    """Import all the submodules of gurps.library so that index functions can see them. Returns a dict mapping names to module objects filled
    with all the submodules of gurps.library (not just the ones that weren't already loaded!)"""
    #Following code from kolypto's answer to http://stackoverflow.com/questions/3365740/how-to-import-all-submodules
    package = sys.modules["gurps.library"]
    return {
        name: importlib.import_module("gurps.library" + '.' + name)
        for loader, name, is_pkg in pkgutil.walk_packages(package.__path__)  # @UnusedVariable
    }

