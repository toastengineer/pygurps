"""gurps.library.core_virtuals
Contains all the virtual traits that are necessary for basic GURPS, but are not part of the Basic Set (since by their very nature they do not
exist in any actual sourcebook), such as the secondary stat bonus attributes."""

import gurps.library.index
gurps.library.index.register_me()
import gurps.characters as characters

#Remember the trait subclass attributes:
#name="[UNNAMED TRAIT]"
#basepointvalue=0 #The actual point value depends on modifiers
#reference="N/A" #The book reference this trait comes from - usually the abbreviated name of the book followed by the page number
#mental=False
#physical=False 
#social=False
#exotic=False
#supernatural=False
#virtual=False 
#Values that differ from the default don't need to be specified (i.e. non virtual traits don't need to define themselves as such; just leave it out)

class SecondaryStatModifier(characters.Trait):
    """The virtual traits PyGURPS uses to represent a character having paid/received points for secondary stats that are different from the values
    that they would normally be given their primary stats."""
    virtual=True
    leveled=True

class ExtraHP(SecondaryStatModifier):
    "Represents that the character has more or less max HP than one would expect from their HT stat."
    
    name="Extra HP"
    basepointvalue=0 
    reference="B16"
    virtual=True
    
    def calculate_hp(self):
        return self.level
    
    @property
    def pointvalue(self):
        return self.level*2

class ExtraWill(SecondaryStatModifier):
    "Represents that the character has more or less Will than one would expect from their IQ stat."
    
    name="Extra Will"
    basepointvalue=0 
    reference="B16"
    virtual=True

    def calculate_will(self):
        return self.level
    
    @property
    def pointvalue(self):
        return self.level*5    
    
class ExtraPerception(SecondaryStatModifier):
    "Represents that the character has more or less Perception than one would expect from their IQ stat."
    
    name="Extra Perception"
    basepointvalue=0
    reference="B16"
    virtual=True
    
    def calculate_perception(self):
        return self.level
    
    @property
    def pointvalue(self):
        return self.level*5    

class ExtraFP(SecondaryStatModifier):
    "Represents that the character has more or less FP than one would expect from their HT score."
    
    name="Extra FP"
    basepointvalue=0 
    reference="B16"
    virtual=True
    
    def calculate_fp(self):
        return self.level
    
    @property
    def pointvalue(self):
        return self.level*3
    
class ExtraBasicSpeed(SecondaryStatModifier):
    """Represents that the character has a higher Basic Speed than one would expect from their stats. Note that one level of this trait
    gives you 0.25 extra Basic Speed."""
    
    name="Extra Basic Speed"
    basepointvalue=0 
    reference="B17"
    virtual=True
    
    def calculate_basic_speed(self):
        return self.level*0.25
    
    @property
    def pointvalue(self):
        return self.level*5

class ExtraBasicMove(SecondaryStatModifier):
    """Represents that the character has a higher Basic Move than one would expect from their stats."""
    
    name="Extra Basic Move"
    basepointvalue=0 
    reference="B17"
    virtual=True
    
    def calculate_basic_move(self):
        return self.level
    
    @property
    def pointvalue(self):
        return self.level*5

class NonZeroSizeModifier(SecondaryStatModifier):
    """Represents that the character has an abnormal (non-zero) SM. SM is worth 0 points."""
    
    name="Non-Zero Size Modifier"
    basepointvalue=0
    reference="B19"
    virtual=True
    pointvalue=0
        
    def calculate_size_modifier(self):
        return self.level
    