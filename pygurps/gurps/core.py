"""Contains stuff that's used all throughout GURPS, such as rolling 3d6, doing success rolls, etc...
Everything in here is brought up to the gurps namespace; that is, you should invoke gurps.skill_roll,
not gurps.core.skill_roll. There's also a lot of little helper functions that are used by other parts
of PyGURPS."""

import random

import collections

class PyGURPSException(Exception):
    "Exceptions defined in PyGURPS are subclasses of this exception."

class PyGURPSValueError(PyGURPSException):
    "Same use-case as vanilla ValueError, but subclassed from PyGURPSException."

class IncompleteTable(dict):
    """A dict whose keys should be ints. Upon being given a nonexistent key,
    it instead gives back the next lowest entry (the entry with the greatest key that is not more than the given number)."""

    def __missing__(self,badkey):
        lowestkey=min(self.keys())
        highestkey=max(self.keys())

        if badkey < lowestkey:
            #If there is no lower entry, raise KeyError
            raise KeyError("Lowest valid key is {0}".format(lowestkey))
        if badkey > highestkey:
            #If it's greater than the highest key, then one entry lower is the highest key
            return self[highestkey]

        #This will recurse until we get a valid key
        #Not really the smartest way to do this, but eh
        return self[badkey-1]

class DispatchDict(dict):
    """A dict subclass specially designed for use as a dispatch dict. This is the Pythonic version of a switch...case statement, with the added
    bonus of being constant-time (normal elif chains are linear-time). It behaves exactly as a normal dict, but is also callable; it is designed
    to be used as a decorator. Calling it with a function object adds that object to the dict. If it is given a key parameter then that key is
    mapped to the given function; otherwise, it defaults to the function's __name__ (the reference defined right after its def statement.)"""

    def __call__(self,func,key=None):
        assert callable(func), "Object given to DispatchDict is not callable."
        if key is None:
            key=func.__name__
        self[key]=func
        return func

class PickyList(list):
    "A list that will only accept items that are in its set of allowed items or instances of classes in its set of allowed items."

    def __init__(self,allowed,*args):
        #We first check that "allowed" is in fact an iterable
        try:
            mybool = None in allowed
        except TypeError:
            #If that raises TypeError, it wasn't. Stuff the given value into a set.
            self.allowed=set(allowed)
        else:
            self.allowed=allowed
        super().__init__(*args)

    def __setitem__(self,key,value):
        if value in self.allowed:
            super().__setitem__(key,value)
        else:
            raise ValueError("Tried to put {0} in a PickyList that only allows {1}".format(value,self.allowed))

    def append(self,value):
        if (value in self.allowed) or (type(value) in self.allowed):
            super().append(value)
        else:
            raise ValueError("Tried to put {0} in a PickyList that only allows {1}".format(value,self.allowed))

    def extend(self,iterable):
        for value in iterable:
            if (value not in self.allowed) and (type(value) not in self.allowed):
                raise ValueError("Tried to put {0} in a PickyList that only allows {1}".format(value,self.allowed))
        super().extend(iterable)

RollResults=collections.namedtuple("RollResults",["succeded","wascritical","margin"])

def diceroll():
    "Roll 3d6, return int result."
    return sum((random.randint(1,6) for t in range(3)))

def roll_under(under):
    "Rolls 3d6, returns true if the result is <= the given integer. Not to be used for skill rolls (ignores all other rules)"
    return diceroll() <= under

def success_roll(under):
    """Similar to roll_under, but returns a tuple of bools; [0] is whether or not the roll was successful (<= under),
    [1] is whether or not it was a critical success/failure. [2] of the tuple is the actual value the diceroll function returned.
    This isn't the function you want for actually rolling against a character's skill or stats either - you probably
    want characters.Character.success_roll().
    """
    roll=diceroll()
    if roll <= 4:
        # <=4 is always crit success
        return RollResults(True, True, under-roll)
    elif roll == 5:
        #5 is crit if effective skill is >= 15
        if under >= 15:
            return RollResults(True, True, under-roll)
        else:
            #otherwise it's just a normal roll
            return RollResults((roll<=under),False,under-roll)
    elif roll == 6:
        #Similarly, 6 is crit if effective skill is >= 16
        if under >= 16:
            return RollResults((roll<=under),True,under-roll)
        else:
            #and likewise
            return RollResults((roll<=under),False,under-roll)
    elif roll <= 16 and roll <= under + 10:
        #A roll less than 17 and skill + 10 may be a non-critical success... we'll see
        if roll <= under:
            #roll under or equal to skill, success
            return RollResults(True, False, under-roll)
        else:
            #roll over skill, fail
            return RollResults(False, False, under-roll)
    elif roll >= under + 10:
        #Roll of skill + 10 or greater is crit fail
        return RollResults(False, True, under-roll)
    elif roll == 17:
        #A roll of 17 is always fail, crit if effective skill is <=15
        if under <= 15:
            return RollResults(False, True, under-roll)
        else:
            return RollResults(False, False, under-roll)
    else:
        #18 is always crit fail, no matter what
        return RollResults(False, True, under-roll)

def string_roll(dpa):
    """Given a string with a roll described in GURPS dice+adds format
    (3d+1 = roll 3 six-sided dice & add 1 to the result) roll it and return the result."""

    rollcount, add = dpa.split("d")
    rollcount=int(rollcount)
    #Note that python will treat the + or - in add as signifying polarity.
    #If there was no add, it'll just be an empty string, which bools false
    if add:
        add=int(add)
    else:
        add=0

    roll=0
    for t in range(rollcount):  # @UnusedVariable
        roll+=random.randint(1,6)
    roll+=add
    return roll

def safe_sum(iterable):
    "Given an iterable, return the sum of its values. This will ignore non-number entries and returns 0 for an empty list or a list of non-numbers."
    retval=0
    for x in iterable:
        try:
            retval+=x
        except TypeError:
            pass
    return retval

def parse_skill_name(name:str):
    """Turn a full skill specifier into a 3-tuple of (name, specialization, tl) (first two as strs, last as int).

    For example:
    "Armoury" becomes ("Armoury", None, None)
    "Armoury (Battlesuits)" becomes ("Armoury", "Battlesuits", None)
    "Armoury (Battlesuits) /TL9" becomes ("Armoury", "Battlesuits", 9)
    """
    #@TODO: The fragility of this function is a little worrying, maybe replace it with a more robust parsing algorithm.
    try:
        tloffst=name.index("/TL")

    except ValueError:
        tl=None
        tloffst=float("inf")

    else:
        tlspec=name[tloffst:].strip()
        #The first three characters are going to be "/TL", so anything from that to the beginning of the specialization
        #will be an int specifying the TL
        tlspec = tlspec.split(" ", maxsplit=1)[0]
        tl=int(tlspec[3:])


    try:
        parenoffst=name.index("(")

    except ValueError:
        specialization=None
        parenoffst=float("inf")

    else:

        try:
            closingparenoffst=name.index(")")
        except ValueError:
            raise PyGURPSValueError("Mismatched parentheses in argument to parse_skill_name ('{0}')".format(name))

        specialization=name[parenoffst:closingparenoffst].strip("()")

    nameend=min(tloffst,parenoffst)
    if nameend == float("inf") : nameend = None

    name = name[:nameend].strip()

    return (name, specialization, tl)

def cascading_partition(thestring, delimiters):
    """Given an iterable of delimiters, call thestring.partition(thedelimiter) with the first delimiter that is actually in the string. Returns None if
    none of the given delimiters are present."""
    for thedelimiter in delimiters:
        if thedelimiter in thestring:
            return thestring.partition(thedelimiter)
    else:
        return None