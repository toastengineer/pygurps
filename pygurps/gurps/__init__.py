'''
PyGURPS
By Schilcote
toastengineer@gmail.com

It is perhaps best to think of PyGURPS as a translation of a handful of the most important GURPS rulebooks into Python source code. It contains
data structures for characters, traits, skills, etc. (gurps.characters), functions representing the core rules of GURPS (these are in the main
module itself), and a module that allows importing .GCS files from the GURPS Character Sheet application. This module is intended to be useful
for creating GURPS-based CRPGs, GURPS character utilities, and any other kind of GURPS-related software you can think of.

GURPS is a trademark of Steve Jackson Games, and its rules and art are copyrighted by Steve Jackson Games. All rights are reserved by Steve Jackson Games.
This game aid is the original creation of Schilcote and is released for free distribution, and not for resale, under the permissions granted in the 
Steve Jackson Games Online Policy.
'''

#Empty __init__.py, yay!
from core import *