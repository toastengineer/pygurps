
#The table found on B168, mapping the TL of the equipment being used to the TL of the user's skill; calculates "off the bottom" values automatically,
#and raises KeyError for differences more than 3; only IQ based skills suffer these penalties, the rest get a flat -1 per TL difference.
class _TLDeltaMap(dict):
    def __missing__(self,key):
        if key > 3:
            raise KeyError("Impossible to substitute a skill for the same skill from its TL + 4 or greater")
        return -7 + ((key + 4) * 2)

iq_skill_tl_delta_to_modifier = _TLDeltaMap({
    3: -15,
    2: -10,
    1: -5,
    0: 0,
    -1: -1,
    -2: -3,
    -3: -5,
    -4: -7
})

if __name__ == '__main__':
    pass