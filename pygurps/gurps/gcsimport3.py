'''
Created on Jun 5, 2015

@author: Schilcote
'''

import os.path
import warnings

import gurps.core
from lxml import etree, objectify  # @UnresolvedImport @UnusedImport Apparently funky things are happening here - this needs to be this way

import gurps.characters as characters
import gurps.library.index as idx

#We need the core virts in order to import anything at all, since those are how we implement secondary traits being modified
import gurps.library.core_virtuals


class PyGURPSGCSImportError(gurps.core.PyGURPSException):
    pass
class PyGURPSGCSImportWarning(UserWarning):
    "Base class for gcsimport3 warnings"
class UnrecognizedSkillWarning(PyGURPSGCSImportWarning):
    "Warning for when an unrecognized skill is read from the .gcs file."
class UnrecognizedModifierWarning(PyGURPSGCSImportWarning):
    "Warning for when an unrecognized trait modifier is read from the .gcs file."
class UnrecognizedTraitWarning(PyGURPSGCSImportWarning):
    "Warning for when an unrecognized trait is read from the .gcs file."
class UnrecognizedMetaTraitWarning(PyGURPSGCSImportWarning):
    "Warning for when an unrecognized metatrait is read from the .gcs file, so we're just importing its contents instead."
class EmptyTraitContainerWarning(PyGURPSGCSImportWarning):
    "Warning for when an empty trait container is read from the .gcs file."


def _get_root_from_file(filehandle):
    """Given a file name, use lxml.objectify to parse the XML tree and return the root element. These are underscored because you're SUPPOSED
    to use the import_character function, but in unusual circumstances (i.e. you have a filehandle, not a path) you'll probably have to call
    these yourself."""
    with filehandle as thefile:
        xmltext=thefile.read()
    root=objectify.fromstring(xmltext)
    return root

def _get_root_from_string(xmltext):
    "Just like _get_root_from_file, except reads directly from a string."
    root=objectify.fromstring(xmltext)
    return root

def _get_active_modifiers(thetraitelement,thetraitclass):
    "Given a trait element, return a list of active modifiers, ready to be put into the trait's .modifiers attribute."
    modifiers=[]
    for themodifierelement in thetraitelement.modifier:
        #Modifier tags have an attribute "enabled" that represents whether or not the modifier is enabled. It's "no" when the modifier isn't, but...
        #to represent an attribute that is enabled, *THE ENABLED ATTRIBUTE IS OMITTED.* Yes, you read that right. It's never "yes," it's just
        #omitted. Wilkes says this is because in his code they default to being enabled. Make of that what you will.
        if "enabled" not in themodifierelement.attrib:
            themod=idx.get_modifier(themodifierelement.name.pyval.strip(), thetraitclass)
            if themod is None:
                warnings.warn("Unrecognized modifier {0} for {1}".format(themodifierelement.name,thetraitclass.name),UnrecognizedModifierWarning)
                continue

            modifiers.append(themod)

    return modifiers

def _get_trait_from_element(thetraitelement):
        "This is meant to be called from _read_trait - that's the function you should use to make a trait from an XML element, not this one."
        newtrait=idx.get_trait(thetraitelement.name.pyval,raise_on_no_results=True)
        try:
            if thetraitelement.levels:
                newtrait.level=thetraitelement.levels.pyval
        except AttributeError:
            #it didn't have a <levels> tag (and thus was not leveled)
            pass
        return newtrait

def _make_perk_or_warn(thetraitelement):
    "This is meant to be called from _read_trait; it simply checks if a trait element is elegible to be converted to a __rk, and if so converts it."
    if hasattr(thetraitelement,"categories"):
        #We use the "category" to flag it as a perk or a quirk (which isn't perfect but is close enough);
        #if the trait HAS no listed categories, then we don't know, maybe it's a taboo trait or something.
        if thetraitelement.categories.category=="Perk":
            #It's a perk - perks have custom names, so let's make a new one
            theperk=gurps.characters.Perk(thetraitelement.name.pyval)
            return theperk
        elif thetraitelement.categories.category=="Quirk":
            #It's a quirk! They're like perks except bad instead of good.
            thequirk=gurps.characters.Quirk(thetraitelement.name.pyval)
            return thequirk

    #Otherwise, it's just a plain ol' unrecognized trait.
    warnings.warn("Encountered unrecognized trait {0}".format(thetraitelement.name),UnrecognizedTraitWarning)

def _read_trait(thetraitelement):
    """The interpretation of traits from the XML is actually kinda complex and needs to be done from multiple places, so we have a function for it."""
    try:
        thetrait=_get_trait_from_element(thetraitelement)
    except idx.TraitNotFoundError:
        #If it's not a known trait, maybe it's a perk/quirk/0-point feature...
        #if it's not, then it's a trait we just plain don't know about.
        thetrait=_make_perk_or_warn(thetraitelement)
        return None

    modifiers=_get_active_modifiers(thetraitelement, thetrait.__class__)
    thetrait.modifiers.extend(modifiers)

    return thetrait

def _read_traitcontainer(thetraitcontainer, thecharacter):
    """Given a trait container, recursively read traits from said trait container and put them on the given character.
    This handles aesthetic trait containers as well as actual metatraits."""
    #If the container is actually a metatrait, we want to handle it just like a regular trait
    #@TODO: support reading in modified metatraits, for example if we have Machine but with Injury Tolerance (No Blood) removed
    if thetraitcontainer.get("type") == "meta trait":
        themetatraitname=thetraitcontainer.name.pyval #type: str
        #GCS likes to append "Meta-Trait: " to the front of the names of metatraits; PyGURPS _doesn't_ do this, so we
        #have to modify the name before looking it up
        themetatraitname=themetatraitname.replace("Meta-Trait: ","")
        themetatrait = newtrait=idx.get_trait(themetatraitname)
        if themetatrait:
            thecharacter.traits.append(themetatrait)
            return
        else:
            warnings.warn("Encountered unrecognized metatrait {0}; importing its contents instead.".format(themetatraitname))
        #if we couldn't add it directly, we just go ahead and add everything that's in it instead.

    #Handle regular traits inside the container...
    try:
        advantages = list(thetraitcontainer.advantage)
    except AttributeError:
        #There were no simple advantages in the container
        pass
    else:
        for thetraitelement in thetraitcontainer.advantage:
            thetrait=_read_trait(thetraitelement)
            #_read_trait will return None if the trait it was supposed to read turned out to be invalid.
            if thetrait is not None:
                thecharacter.traits.append(thetrait)

    #... as well as nested containers.
    try:
        containers = list(thetraitcontainer.advantage_container)
    except AttributeError:
        #The container contained no containers
        pass
    else:
        for thecontainer in containers:
            _read_traitcontainer(thecontainer,thecharacter)

def _build_character_from_root(rootelement):
    """Given an lxml.objectify root element object representing the root <character> element of a version 2 .GCS save file, return a
    gurps.characters.Character representing the same GURPS character."""

    thecharacter=characters.Character()

    thecharacter.name=rootelement.profile.name

    #Base stats are direct children of the root
    thecharacter.st=rootelement.ST
    thecharacter.dx=rootelement.DX
    thecharacter.iq=rootelement.IQ
    thecharacter.ht=rootelement.HT

    #Of course, because of the way PyGURPS represents secondary stats, converting those will be a little harder...

    #Secondary stats are stored in the XML as modifiers to their unmodified values; we just stick those in a trait.
    #If there actually is a nonzero modifier to the "normal" HP value...
    if rootelement.HP:
        hpmod=idx.get_trait("Extra HP")
        hpmod.level=rootelement.HP.pyval
        thecharacter.traits.append(hpmod)

    if rootelement.FP:
        fpmod=idx.get_trait("Extra FP")
        fpmod.level=rootelement.FP.pyval
        thecharacter.traits.append(fpmod)

    if rootelement.will:
        willmod=idx.get_trait("Extra Will")
        willmod.level=rootelement.will.pyval
        thecharacter.traits.append(willmod)

    if rootelement.perception:
        permod=idx.get_trait("Extra Perception")
        permod.level=rootelement.perception.pyval
        thecharacter.traits.append(permod)

    if rootelement.speed:
        bsmod=idx.get_trait("Extra Basic Speed")
        #Since EBS is the only modifier trait where 1 level does NOT get you +1 bonus,
        #we have to convert the actual bonus (which is what's in the XML) to levels by multiplying by 4 (since 1 level gets you 0.25 bonus to the stat).
        bsmod.level=int(rootelement.speed * 4)
        thecharacter.traits.append(bsmod)

    if rootelement.move:
        bmmod=idx.get_trait("Extra Basic Move")
        bmmod.level=rootelement.move.pyval
        thecharacter.traits.append(bmmod)

    if rootelement.profile.SM:
        bmmod=idx.get_trait("Non-Zero Size Modifier")
        bmmod.level=rootelement.profile.SM.pyval
        thecharacter.traits.append(bmmod)

    #Next we handle skills
    for theskill in rootelement.skill_list.skill:
        skillspec=theskill.name.pyval
        if hasattr(theskill,"tech_level"):
            skillspec+=" /TL{0}".format(theskill.tech_level)
        if hasattr(theskill,"specialization"):
            skillspec+=" ({0})".format(theskill.specialization)

        #We set our converted skill to be at rlevel 0; GCS stores skills
        #in terms of how many points were spent on them so we just use the .pointvalue property to set that.
        convertedskill=idx.get_skill(skillspec, 0, thecharacter)
        if convertedskill is None:
            warnings.warn("Unknown skill {0}".format(skillspec),UnrecognizedSkillWarning)
            continue
        convertedskill.pointvalue=theskill.points.pyval

        thecharacter.skills.append(convertedskill)

    #And finally, traits!
    for thetraitelement in rootelement.advantage_list.advantage:
        try:
            thetrait=_read_trait(thetraitelement)
            #_read_trait will return None if the trait it was supposed to read turned out to be invalid.
            if thetrait is not None:
                thecharacter.traits.append(thetrait)
        except AttributeError as e:
            #If reading advantages from <advantage_list> fails with AttributeError that means there's no advantage tags under it-
            #in that case we don't need to do anything
            print(e.__dict__)
            pass

    #Metatraits and purely-graphical "grouping" of traits are defined in the .gcs XML as "advantage containers";
    #since they can be nested we need to handle them recursively.
    try:
        containers=list(rootelement.advantage_list.advantage_container)
    except AttributeError: #there were none
        pass
    else:
        for thecontainer in containers:
            _read_traitcontainer(thecontainer,thecharacter)

    return thecharacter

def import_character(filename):
    """Given the complete path to a version 2 .GCS XML file produced by GURPS Character Sheet, return a gurps.characters.Character object representing
    the character contained within that file."""
    root=_get_root_from_file(open(filename,"rb"))
    character=_build_character_from_root(root)
    return character

if __name__ == '__main__':
    idx.load_all_library_modules()
    objectify.enable_recursive_str()
    root=_get_root_from_file(open(os.path.join(".","PyGURPS Test Character.gcs"),"rb"))
    thechar=_build_character_from_root(root)

    #Wait for all the warnings to print
    import time
    time.sleep(0.2)

    print(thechar)
    print(thechar.traits)
    for theskill in thechar.skills:
        print(theskill.fullname)