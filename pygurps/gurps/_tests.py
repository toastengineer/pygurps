'''
Created on Jan 1, 2015

@author: Schilcote
'''
import os.path
import random
import unittest
import pkgutil


#CORE TESTS
class CoreTests(unittest.TestCase):

    def test_roll_and_last_roll(self):
        import gurps.core as core
        for t in range(0,1000):  # @UnusedVariable
            result=core.diceroll()
            assert isinstance(result,int)
            assert 3 <= result <= 18

    def test_skill_roll(self):
        import gurps.core as core
        for t in range(0,1000):
            level=int(t % 20)
            result=core.success_roll(level)
            assert isinstance(result, tuple)
            assert len(result)==3
            assert isinstance(result[0], bool)
            assert isinstance(result[1], bool)

            succeded, critted, margin = result
            roll = level - margin
            if 4 <= roll >= 18: # 3s & 4s & 18s are ALWAYS crits
                assert critted,(level,result)
                #note that we don't really thoroughly test the crit rules
            if (roll <= level or roll <= 4) and roll < 17: #3s & 4s always succeed
                assert succeded,(level,result)
            else:
                #17s and 18s always fail
                    assert not succeded,(level,result)


    def test_safe_sum(self):
        import gurps.core as core
        assert core.safe_sum([1,2,3]) == 6
        assert core.safe_sum([1,2,None,"Hello World",3]) == 6
        assert core.safe_sum([None,None,None]) == 0

    def test_picky_list_values_allowed(self):
        import gurps.core as core
        pickylist=core.PickyList((1,2,3))
        pickylist.append(1)
        pickylist.append(2)
        pickylist.append(3)

    def test_picky_list_values_not_allowed(self):
        import gurps.core as core
        pickylist=core.PickyList((1,2,3))
        try:
            pickylist.append(4)
        except ValueError as e:
            assert str(e) == "Tried to put 4 in a PickyList that only allows (1, 2, 3)"
        else:
            raise AssertionError("PickyList accepted 4 when it was configured to accept 1 through 3!")

    def test_picky_list_instances_allowed(self):
        import gurps.core as core
        pickylist=core.PickyList((int,bool))
        pickylist.append(1)
        pickylist.append(2)
        pickylist.append(4)
        pickylist.append(8)
        pickylist.append(True)
        pickylist.append(False)

    def test_picky_list_instances_not_allowed(self):
        import gurps.core as core
        pickylist=core.PickyList((int,bool))
        try:
            pickylist.append("This'll fail!")
        except ValueError as e:
            assert str(e) == "Tried to put This'll fail! in a PickyList that only allows (<class 'int'>, <class 'bool'>)"
        else:
            raise AssertionError("PickyList accepted a string when it was configured to accept ints and bools!")

class IndexTestsBSLoaded(unittest.TestCase):

    def test_basic_set_registered(self):
        import gurps.library.basic_set as bs
        import gurps.library.index as idx
        assert bs in idx.librarymodules

    def test_basic_set_loaded_retall(self):
        import gurps.library.index
        import gurps.library.basic_set# @UnusedImport

        result=gurps.library.index.look_up(returnall=True)  # @UnusedVariable
        #This can't actually be tested, since the test would have to be rewritten every time basic_set changes. It works, I swear!
        assert result, result

    def test_superclass_filter(self):
        import gurps.library.index
        import gurps.library.basic_set # @UnusedImport
        import gurps.characters as characters

        result=gurps.library.index.look_up(superclasses=[characters.Trait],returnall=True)
        #Same problem as above.
        for thetrait in result:
            assert issubclass(thetrait, characters.Trait)
        assert result,result

    def test_attribute_filter(self):
        import gurps.library.index
        import gurps.library.basic_set

        result=gurps.library.index.look_up(attrib_name="Striking ST")
        assert result==gurps.library.basic_set.StrikingST,result

        result=gurps.library.index.look_up(attrib_reference="B34")
        assert result==gurps.library.basic_set.ThreeSixtyVision,result

    def test_get_trait(self):
        import gurps.library.index
        import gurps.library.basic_set

        result=gurps.library.index.get_trait("Lifting ST (1)")
        assert isinstance(result,gurps.library.basic_set.LiftingST), result
        assert result.level == 1, result.level

    def test_get_trait_non_existent(self):
        import gurps.library.index
        import gurps.library.basic_set  # @UnusedImport

        result=gurps.library.index.get_trait("Spam, spam, spam, and spam.")
        assert result is None

    def test_get_skill_basic(self):
        import gurps.library.index
        import gurps.library.basic_set

        result=gurps.library.index.get_skill("Accounting")
        assert isinstance(result,gurps.library.basic_set.Accounting)

    def test_get_skill_technical(self):
        import gurps.library.index
        import gurps.library.basic_set

        result=gurps.library.index.get_skill("Alchemy/TL7")
        assert isinstance(result,gurps.library.basic_set.Alchemy)
        assert result.tl==7

    def test_get_skill_specialized(self):
        import gurps.library.index
        import gurps.library.basic_set

        result=gurps.library.index.get_skill("Animal Handling (Pythons)")
        assert isinstance(result,gurps.library.basic_set.AnimalHandling)
        assert result.specialization=="Pythons"

    def test_get_skill_both(self):
        import gurps.library.index
        import gurps.library.basic_set

        result=gurps.library.index.get_skill("Armoury/TL7 (Battlesuits)")
        assert isinstance(result,gurps.library.basic_set.Armoury), result
        assert result.tl==7, result.tl
        assert result.specialization=="Battlesuits", result.specialization

    def test_get_skill_non_existent(self):
        import gurps.library.index
        import gurps.library.basic_set  # @UnusedImport

        result=gurps.library.index.get_skill("Ham, eggs, ham, spam, and spam.")
        assert result is None, result

class GCSImport3Tests(unittest.TestCase):

    def test_importing_test_character(self):
        import gurps.gcsimport3
        root=gurps.gcsimport3._get_root_from_string(pkgutil.get_data("gurps", "PyGURPS Test Character.gcs"))
        character=gurps.gcsimport3._build_character_from_root(root)

class ExhaustiveBookModuleTests(unittest.TestCase):
    #Just pull in EVERYTHING from EVERY book module and try to instantiate it.

    def test_all_skills(self):
        import gurps.library.index
        import gurps.characters
        gurps.library.index.load_all_library_modules()
        allskills=gurps.library.index.look_up(superclasses=[gurps.characters.Skill],returnall=True)
        assert len(allskills) > 0

        for theskill in allskills:
            #@TODO: What?
            if theskill.name=="Animal Handling":
                pass
            instantiateparams={"rlevel":0,"owner":None}
            if theskill.technical:
                instantiateparams["tl"]=8
            if theskill.mustspecialize:
                instantiateparams["specialization"]="Bogus Specialization"
            theskill(**instantiateparams)

    def test_all_traits(self):
        import gurps.library.index
        import gurps.characters
        gurps.library.index.load_all_library_modules()
        alltraits=gurps.library.index.look_up(superclasses=[gurps.characters.Trait],returnall=True)
        assert len(alltraits) > 0
        for thetrait in alltraits:
            if thetrait.requiresparenthetical:
                if thetrait.acceptedparentheticals:
                    for theparenthetical in thetrait.acceptedparentheticals:
                        thetrait(parenthetical=theparenthetical)
                else:
                    theparenthetical="Foo"
                thetrait(parenthetical=theparenthetical)
            else:
                thetrait()

class CharacterTests(unittest.TestCase):

    def test_basic_instantiation(self):
        import gurps.characters
        thecharacter=gurps.characters.Character("Test Character")
        assert thecharacter.point_value()==0, thecharacter
        assert thecharacter.name=="Test Character", thecharacter
        assert thecharacter.st==10, thecharacter
        assert thecharacter.dx==10, thecharacter
        assert thecharacter.iq==10, thecharacter
        assert thecharacter.ht==10, thecharacter

    def test_character_string_conversion(self):
        """This also ends up running most of the other parts of the Character, since str()ing a Character involves
        working out most of its attributes."""
        import gurps.characters
        thecharacter=gurps.characters.Character("Test Character")
        str(thecharacter)

    def test_fullname_to_findskill_round_trip(self):
        import gurps.characters, gurps.library
        gurps.library.index.load_all_library_modules()
        allskills=gurps.library.index.look_up(superclasses=[gurps.characters.Skill],returnall=True)
        mychar=gurps.characters.Character()

        for theskill in allskills:
            instantiateparams={"rlevel": 0,"owner": mychar}
            if theskill.technical:
                instantiateparams["tl"]=8
            if theskill.mustspecialize:
                instantiateparams["specialization"]="Bogus Specialization"
            mychar.skills.append(theskill(**instantiateparams))


        for theskill in mychar.skills:
            assert mychar.find_skill(theskill.fullname) == theskill

    def test_rolling_against_every_skill(self):
        import gurps.characters,gurps.library
        gurps.library.index.load_all_library_modules()
        allskills=gurps.library.index.look_up(superclasses=[gurps.characters.Skill],returnall=True)
        mychar=gurps.characters.Character()

        for theskill in allskills:
            instantiateparams={"rlevel": 0,"owner": mychar}
            if theskill.technical:
                instantiateparams["tl"]=8
            if theskill.mustspecialize:
                instantiateparams["specialization"]="Bogus Specialization"
            mychar.skills.append(theskill(**instantiateparams))

        for theskill in mychar.skills:
            mychar.success_roll(theskill.fullname)

    def test_rolling_against_every_trait(self):
        import gurps.characters
        mychar=gurps.characters.Character()
        for thetrait in {"st", "iq", "dx", "ht", "will", "perception"}:
            mychar.success_roll(thetrait)


if __name__ == "__main__":
    unittest.main()