'''
Created on Jan 3, 2015

@author: Schilcote
'''
from setuptools import setup
setup(name='pyGURPS',
      version='0.0',
      description='GURPS role-playing-game library',
      author='Schilcote',
      author_email='toastengineer@gmail.com',
      packages=['gurps', 'gurps.library'],
	  )